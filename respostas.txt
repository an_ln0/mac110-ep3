/***** MAC0110 - EP3 *****/
  Nome: Antonio Alves do Nascimento Carvalho
  NUSP: 9848878

/***** Parte 1 - Entendendo o código *****/

    1) Qual a probabilidade de um elemento da matriz ser grama (ou terreno)? Você pode expressar essa possibilidade utilizando um valor numérico ou uma fórmula.

        A probabiliade é o evento dividido pelo espaço amostral o evento é o terreno, e o espaço amostral é a quantidade de elementos  da matriz vezes a quantidade que cada elemento pode assumir, assim a probabilidade de um elemento da matriz ser terreno é:

    P = 1/[(quantidade de elementos da matriz)*(5)]
onde 5 representa as cinco variações possivei para um elemento da matriz, ou seja, cenoura,terreno,lobo,coelho e terreno especial.

se substituirmos quantidade de matriz por 20 que é o tamanho do terreno, chegamos a P = 1/(5*20) = 0,01.

 
    2) Analise o código e diga: qual a diferença entre o terreno e o terreno especial?

        Não existe uma diferença entre terreno e terreno especial, os dois possuem a mesma probabilidade, além disso geram o   mesmo alimento com a mesma quantidade de energia e se regeneram do mesmo modo, portanto, a única diferença é o simbolo.

    3) Dados os valores iniciais das constantes, qual a energia necessária para um lobo se reproduzir? E um coelho?

        Como a energia para se reproduzir é o FATOR_REPRODUCAO * animal_energia, com os dados iniciais o valor para a reprodução do lobo é 10 que é a energia inicial vezes 2 que é o FATOR_REPRODUCAO assim, o lobo precisa de 20 de energia para se reproduzir, de modo analogo o coelho possui energia inicial de 6, portanto com essa energia inicial,o coelho vai precisar de 12 de energia para se reproduzir, porém a animal_energia varia a medida da execução do programa.  

/***** Parte 2 - Completando as funções *****/

    4) Ao contrário das funções ocupa_vizinho! e reproduz!, a função morre! não recebe a matriz de energia como parâmetro. Por quê não é necessário alterar a energia com a morte de um animal?

        Porque o fato de morrer não depende da interação com outros componentes, assim somente muda o componente que morreu sem interagir com outros elementos. 

/***** Parte 3 - Teste e simulação *****/

    5) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece na ilha?

        Considerando os valores iniciais os coelhos deixam de existir, assim só retam lobos terreno, terreno especial e cenouras.

    6) Qual combinação de constantes leva a ilha a ser dominada por coelhos? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

        Utilizando 90 iterações e considerando:
        
           PROBABILIDADE_LOBO = 0.001
           PROBABILIDADE_COELHO = 0.008
           PROBABILIDADE_COMIDA = 0.007
           REGENERACAO_TERRENO = 0.008
           PROBABILIDADE_ESPECIAL = 0.008
           FATOR_REPRODUCAO = 3
           TAMANHO_ILHA = 40
         
        Se chegou a uma ilha com 10 resultados seguido sem lobos, assim podemos concluir que esses dados na maioria das vezes chegam a uma ilha sem lobos. 
  

    7) Qual combinação de constantes leva a ilha não ter nenhum animal? Utilize uma ilha de tamanho 20 ou mais, e simule utilizando uma quantidade considerável de iterações.

	Com as seguintes requisitos: 

        PROBABILIDADE_LOBO = 0.009
	PROBABILIDADE_COELHO = 0.001
	PROBABILIDADE_COMIDA = 0.001
	REGENERACAO_TERRENO = 0.001
	PROBABILIDADE_ESPECIAL = 0.001
	FATOR_REPRODUCAO = 4
	TAMANHO_ILHA = 40

	Foi possível obter uma ilha sem a presença de coelho e também de lobos, com esse resultado se repetindo 10 vezes,com isso podemos concluir que uma ilha com essas características resulta em uma ilha sem animais.

/***** Parte 3 - Usando DataFrames e plotando gráficos *****/

    8) Qual a diferença entre a função simula - da parte 3 - e a função simula2?

        Na função simula, cada vez que a função é chamada, ocorre a chamada de outras funções, então nessa função se começa com uma ilha que vai sofrendo alterações.
	Na função simula2, a ilha é atualizada porém as informações vão sendo armazenadas, no que parece ser um vetor. 

    9) A função gera_graficos possui sintaxes diferentes das vistas em aula e nos outros exercícios. Apesar disso, é possível entender o que a função faz, sem rodá-la e sem conhecer detalhes sobre os pacotes de gráficos. Sem rodar a função, responda: quantos gráficos a função plota? Qual o conteúdo de cada gráfico?

       A função parece gerar 2 graficos, um relacionado a quantiidade de lobos e coelhos e outros relacionado a energia total e comida.

    10) Usando os valores iniciais das constantes e após uma quantidade considerável de iterações, mais de 30, por exemplo, o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu na parte 3?

        <RESPONDA AQUI>

    11) Usando a combinação de constantes da questão 6 - que leva a ilha a ser dominada por coelhos - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        <RESPONDA AQUI>

    12) Usando a combinação de constantes da questão 7 - que leva a ilha à extinção de todos os animais - o que acontece nos gráficos mostrados? Houve alguma semelhança ou diferença no que você respondeu naquela questão?

        <RESPONDA AQUI>
